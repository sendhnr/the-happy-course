const bcrypt = require('bcrypt')

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

     await queryInterface.bulkInsert('Users', [{
         name: 'John Doe',
         role: 'Pengajar',
         email: 'falonez@gmail.com',
         password: bcrypt.hashSync('falonez', 10),
         programsId: 0,
         createdAt: new Date(),
         updatedAt: new Date(),
       }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
