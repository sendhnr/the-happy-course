'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Programs extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Programs.hasMany(models.Users, {
        foreignKey: 'programsId'
      })
    }
  }
  Programs.init({
    name: DataTypes.STRING,
    periodStart: DataTypes.DATE,
    periodEnd: DataTypes.DATE,
    teacher: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Programs',
  });
  return Programs;
};